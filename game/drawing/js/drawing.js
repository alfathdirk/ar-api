(function () {
  var selectedColor = '#ff01fe';
  var nameImagePath = window.location.search.substr(1).split('=').pop();
  var canvasWidth = window.innerWidth,
    canvasHeight = window.innerHeight / 1.6;

  var calculateAspectRatioFit = function (srcWidth, srcHeight) {
    var ratio = Math.min(canvasWidth / srcWidth, canvasHeight / srcHeight);
    return {
      width: srcWidth * ratio,
      height: srcHeight * ratio
    };
  }

  var $_ = function (id) {
    return document.getElementById(id)
  };

  $_('c').setAttribute('width', canvasWidth);
  $_('c').setAttribute('height', canvasHeight);

  var canvas = this.__canvas = new fabric.Canvas('c', {
    isDrawingMode: true,
  });

  $('.btn-close').click(function() {
    window.postMessage('close');
  });

  canvas.backgroundColor = '#fff';
  $('.color-list .color-item').on('click', function () {
    $('.color-list .color-item').removeClass('active');
    $(this).addClass('active');
    let color = {
      'color-1': '#f70001',
      'color-2': '#ff01fe',
      'color-3': '#d859d7',
      'color-4': '#fd8000',
      'color-5': '#f1f106',
      'color-6': '#006100',
      'color-7': '#79f100',
      'color-8': '#1f01ef',
      'color-9': '#3197fe',
      'color-10': '#7f00fd',
      'color-11': '#8a4e09',
      'color-12': '#181818',
    }
    let colorItem = $(this).attr('class').split(' ')[1];
    canvas.freeDrawingBrush.color = color[colorItem];
    selectedColor = color[colorItem];
  });

  fabric.Object.prototype.transparentCorners = false;

  if (fabric.PatternBrush) {
    var squarePatternBrush = new fabric.PatternBrush(canvas);
    squarePatternBrush.getPatternSrc = function () {

      var squareWidth = 3,
        squareDistance = 1;

      var patternCanvas = fabric.document.createElement('canvas');
      patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
      var ctx = patternCanvas.getContext('2d');
      ctx.fillStyle = this.color;
      ctx.fillRect(0, 0, squareWidth, squareWidth);
      return patternCanvas;
    };
  }


  $('.tab__check').on('click', function () {
    let activeState = $(this).val();
    if (activeState === 'save') {
      fabric.Image.fromURL('./drawing/img/watermark.png', function (myImg) {
        let scale = 120 / myImg.width;
        let obj = {
          scaleX: scale,
          scaleY: scale,
          left: 20,
          // top: canvasHeight - 45,
        };
        let img1 = myImg.set(obj);
        // canvas.setOverlayImage('./drawing/img/watermark.png', canvas.renderAll.bind(canvas), obj);
        canvas.add(img1);
      })
      setTimeout(function() {
        console.log(canvas.toDataURL('jpg'));
        window.postMessage(canvas.toDataURL('jpg'));
      }, 500);
    }
    if (activeState !== 'on' && activeState !== 'eraser') {
      $('.tab__bodys').removeAttr('style');
      $(`#${activeState}`).css("display", "block");
    }

    if (activeState === 'crayon') {
      canvas.freeDrawingBrush = squarePatternBrush;
    } else {
      let v = this.value;
      if (activeState === 'eraser') {
        v = 'Pencil';
      }
      canvas.freeDrawingBrush = new fabric[ v + 'Brush'](canvas);
    }

    if (canvas.freeDrawingBrush) {
      console.log(activeState);
      canvas.freeDrawingBrush.color = activeState === 'eraser' ? '#fff' : selectedColor;
      canvas.freeDrawingBrush.width = 8 || 1;
      // canvas.freeDrawingBrush.shadow = new fabric.Shadow({
      //   blur: 2 || 0,
      //   offsetX: 0,
      //   offsetY: 0,
      //   affectStroke: true,
      //   color: '#fff',
      // });
    }
  });



  // $_('drawing-mode-selector').onchange = function () {


  // };

  if (canvas.freeDrawingBrush) {
    canvas.freeDrawingBrush.color = selectedColor;
    canvas.freeDrawingBrush.width = 8;
    // canvas.freeDrawingBrush.shadow = new fabric.Shadow({
    //   blur: 2 || 0,
    //   offsetX: 0,
    //   offsetY: 0,
    //   affectStroke: true,
    //   color: '#ccc',
    // });
  }

  fabric.Image.fromURL(`${window.location.origin}/app/assets/${nameImagePath}`, function (myImg) {
    let scale = canvasWidth / myImg.width;
    canvas.setOverlayImage(`${window.location.origin}/app/assets/${nameImagePath}`, canvas.renderAll.bind(canvas), {
      scaleX: scale,
      scaleY: scale,
    });
  });


})();
