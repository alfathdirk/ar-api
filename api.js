const Bundle = require('bono');
const multer = require('@koa/multer');
const koaBody = require('koa-body');
const manager = require('./middlewares/database');
const { BASE_URL_GAME } = require ('./config');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    let dir = './uploads'
    // console.log('req',req);
    if(req.files.image) {
      dir = './uploads/product';
    }

    cb(null, dir);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  }
})

const upload = multer({ storage });

class Api extends Bundle {
  constructor() {
    super();
    // this.use(koaBody({ multipart: true }));
    this.get('/list-game', this.getGame.bind(this));
    this.post('/add-game', this.postNewGame.bind(this));
    this.post('/add-category', this.postCategory.bind(this));
    this.get('/list-category', this.getCategory.bind(this));
  }

  getGame(ctx) {
    return manager.runSession(async (session) => {
      let game = await session.factory('Games').all();
      return game;
    });
  }

  getCategory(ctx) {
    return manager.runSession(async (session) => {
      let game = await session.factory('Product').sort({ id: 0 }).all();
      return game;
    });
  }

  async postCategory(ctx) {
    let data = await upload.fields([
      {
        name: 'image',
        maxCount: 1,
      }
    ])(ctx);
    let { name, description } = ctx.request.body;
    let image = ctx.files.image[0].filename;
    let objsave = {
      name,
      image,
      description,
      createdDate: new Date().getTime(),
    };
    return manager.runSession(async (session) => {
      await session.factory('Product').insert(objsave).save();
      return objsave
    })
  }

  async postNewGame(ctx) {
    let result = await manager.runSession(async (session) => {
      let data = await upload.fields([
        {
          name: 'photoScan',
          maxCount: 1,
        },
        {
          name: 'gameData',
          maxCount: 2
        },
      ])(ctx);

      let fileUpload = Object.keys(ctx.request.files).length;
      let name = ctx.request.body.name;
      let gameType = ctx.request.body.gameType;

      if (!name) {
        throw new Error('Nama game harus diisi');
      }

      let { inserted, rows } = await session.factory('Games').insert({
        targetName: name,
        urlPhotoScan: ctx.files.photoScan[0].filename,
        gameData: !gameType ? ctx.files.gameData[0].filename : gameType,
        gameType: !gameType ? 1 : 2
      }).save();

      return {
        name,
        files: ctx.files,
      };
    });
    return result;
  }
}

module.exports = new Api();
