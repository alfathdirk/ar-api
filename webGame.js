const Bundle = require('bono');
const serve = require('koa-static');

class Auth extends Bundle {
  constructor() {
    super();
    this.get('/', this.index.bind(this));
  }

  async index (ctx) {
    return {
      hello: 'world'
    };
  }
}
module.exports = new Auth();
